 [![pipeline status](https://git.dkfz.de/a290/containers/spatial-segmentation/badges/main/pipeline.svg)](https://git.dkfz.de/a290/containers/spatial-segmentation/-/commits/main) 
 [![coverage report](https://git.dkfz.de/a290/containers/spatial-segmentation/badges/main/coverage.svg)](https://git.dkfz.de/a290/containers/spatial-segmentation/-/commits/main) 
 [![Latest Release](https://git.dkfz.de/a290/containers/spatial-segmentation/-/badges/release.svg)](https://git.dkfz.de/a290/containers/spatial-segmentation/-/releases)

# Container for analysing Resolve Molecular Cartography data

